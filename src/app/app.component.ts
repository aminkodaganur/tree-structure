import { Component } from '@angular/core';

import { TreeModel, NodeEvent, Tree, Ng2TreeSettings } from 'ng2-tree';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent {
	title = 'app';

	public treeSettings: Ng2TreeSettings = {
		rootIsVisible: true
	};

	public teamData: TreeModel = {
		value: 'Team',
		children: [
			{
				value: 'Sub-team 1',
				children: [{ value: 'John' }, { value: 'Harry' }, { value: 'Petter' }],
				settings:{
					isCollapsedOnInit : true
				}
			},
			{
				value: 'Sub-Team 2',
				children: [{ value: 'Johnny' }, { value: 'Mark' }, { value: 'Linda' }],
				settings:{
					isCollapsedOnInit : true
				}
			}
		],
		settings:{
			isCollapsedOnInit : true
		}
	};

	public groupData: TreeModel = {
		value : 'Play Group',
		children: [
			{
				value: 'Football',
				children: [ {value : 'GK'} , {value: 'CF'} , {value : 'RMO'} , {value : 'LMO'} ],
				settings:{
					isCollapsedOnInit : true
				}
			}
		],
		settings:{
			isCollapsedOnInit : true
		}
	};

	public logEvent(e: NodeEvent): void {
		console.log(e);
	}
}
